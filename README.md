# A basic Rack application

## Install

To install the dependencies, run:

    $ bundle install

## Starting the server

To launch the application, run:

    $ bundle exec rackup -p 9292

If you want to reload the application each time a file is changed, run:

    $ bundle exec rerun 'rackup -p 9292'

Those commands will run the server on port 9292.

## Send queries to the server

Using curl, run:

    $ curl -i 'http://localhost:9292/'

The `-i` option stands for `include` and it includes the HTTP headers of the response.

To try a different HTTP verb, run:

    $ curl -i -X POST 'http://localhost:9292/'

For more information about curl, try [this article](http://httpkit.com/resources/HTTP-from-the-Command-Line/)
