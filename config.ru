require "pry"
require "sqlite3"
require "active_record"

# Open a database
ActiveRecord::Base.establish_connection(adapter: "sqlite3", database: "test.db")

# class Counter -> table counters
class Counter < ActiveRecord::Base
end

class App
  def call(env)
    # Uncomment to get pry before each request
    # binding.pry

    request = Rack::Request.new(env)
    name = request.params["name"]

    # rows = DB.execute("select * from counters order by val desc limit 1")
    # counter = rows.first.first || 0
    counter = Counter.order(val: :desc).first || 0

    body = "<h1>Hello #{counter.val}</h2>"

    # DB.execute("insert into counters (val) values (#{counter + 1})")
    Counter.create(val: counter.val + 1)

    [
      200,                                 # status code
      { 'Content-Type' => 'text/html' },   # headers
      [ body ],                            # body
    ]
  end
end

run App.new
