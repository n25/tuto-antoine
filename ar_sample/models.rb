require_relative "database"

class Post < ActiveRecord::Base
  has_and_belongs_to_many :authors
end

class Comment < ActiveRecord::Base
  belongs_to :author
end

class Author < ActiveRecord::Base
  has_and_belongs_to_many :posts
  has_many :comments
end
