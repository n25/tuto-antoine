require "faker"

require_relative "database"

ActiveRecord::Migration.class_eval do

  create_table :posts do |t|
    t.string :title
    t.text   :body

    t.timestamps
  end

  create_table :authors do |t|
    t.string :first_name
    t.string :last_name
    t.string :short_name
  end

  create_table :authors_posts do |t|
    t.integer :author_id
    t.integer :post_id
  end

  create_table :comments do |t|
    t.integer :author_id
    t.text    :body

    t.timestamps
  end

end

require_relative "models"

authors = (1..15).to_a.map do |i|
  Author.create({
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    short_name: "Author ##{i}",
  })
end

100.times do
  author_count = 1 + rand(3)
  post = Post.create({
    title: Faker::Lorem.sentence(3, false, 2),
    body: Faker::Lorem.paragraph(5),
    authors: authors.sample(author_count),
  })

  rand(30).times do
    Comment.create({
      author: authors.sample,
      body: Faker::Lorem.paragraph(3),
    })
  end
end
