# Co-authoring blogging platform

## Install the required libraries

    bundle install

## Generate the database

You can generate the file `./ar_sample.db` running the following command:

    bundle exec ruby ./ar_sample/seed.rb

Since the creation of the data isn't optimized and since it is SQLite, it
will take quite some time to do, don't worry.

## Opening a console

To open a console and play with the models, type:

    bundle exec pry -r "./ar_sample/models"

# What you can do

## Understand the structure of the database

To do this, you can check the `./ar_sample/seed.rb` file. You can use `sqlitebrowser` to
navigate the database and see what the table and columns are.

## Compute some values using ActiveRecord and Ruby

### The best contributor to our site

We want to know who is the author that is the most active on the platform because we want to reward him.

To do that we'll assume that each author will be ranked:

- 1 point per comment authored
- 2 points per post co-authored

Giving those rules, fin the best contributor.

*Going further* try to add the following rule to the others:

- 3 points per post authored alone
