require_relative "models"
require "pry"

# This will log the queries on the standard output.
ActiveRecord::Base.logger = Logger.new(STDOUT)

#
# Author with the best contribution score
#

# Ruby computation

counters = Hash.new { |hash, key| hash[key] = 0 }

Post.preload(:authors).each do |post|
  post.authors.each do |author|
    counters[author] += 2
  end
end

Comment.preload(:author).each do |comment|
  counters[comment.author] += 1
end

best_author = counters.map { |author, score| [score, author] }
                .sort_by(&:first).last.last

# SQL computation

best_author_id = ActiveRecord::Base.connection.execute(<<-SQL).first["author_id"]
  select
    t.author_id,
    comment_score + sum(2) as score
  from (
    select
      authors.id as author_id,
      sum(1) as comment_score
    from authors
    left join comments
      on authors.id = comments.author_id
    group by authors.id
  ) as t
  left join authors_posts
    on authors_posts.author_id = t.author_id
  group by t.author_id
  order by score desc
  limit 1
SQL
